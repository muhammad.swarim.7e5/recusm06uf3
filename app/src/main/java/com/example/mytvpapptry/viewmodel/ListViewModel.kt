package com.example.mytvpapptry.viewmodel

import androidx.lifecycle.asLiveData
import com.example.mytvpapptry.ServiceLocator

class ListViewModel : ViewModel() {
    val itemsDao = ServiceLocator.itemsDao
    val items = itemsDao.listFlow().asLiveData()
}