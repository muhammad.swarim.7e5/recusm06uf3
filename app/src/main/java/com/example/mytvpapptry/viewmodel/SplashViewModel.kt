package com.example.mytvpapptry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mytvpapptry.ServiceLocator
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Actividad Splash
 * @property TIME Long
 */
class SplashViewModel : ViewModel() {
    private val Time: Long = 1000000000
    val loggedIn = MutableLiveData<Boolean>()

    fun start() {
        if (ServiceLocator.realmManager.loggedIn()) {
            CoroutineScope(Dispatchers.IO).launch {
                ServiceLocator.realmManager.configureRealm()
                loggedIn.postValue(true)
            }
        } else {
            loggedIn.postValue(false)
        }
    }

}