package com.example.mytvpapptry.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mytvpapptry.Categories
import com.example.mytvpapptry.ServiceLocator
import com.example.mytvpapptry.ServiceLocator.itemsDao

import com.example.mytvpapptry.databinding.ActivityMainBinding
import com.example.mytvpapptry.models.Position.MarkerModel
import com.example.mytvpapptry.models.Position.Position
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

open class ViewModel : ViewModel() {
    var displayMarkerList = MutableLiveData<List<MarkerModel>>(listOf())
    var rawMarkerList = MutableLiveData<MutableList<MarkerModel>>(mutableListOf())
    var filterCategory = MutableLiveData<Categories?>(null)
    var detailMarker = MutableLiveData<MarkerModel>()
    var selectedPosition = MutableLiveData<Position>()
    var mBinding = MutableLiveData<ActivityMainBinding>()
    val loggedIn = MutableLiveData<Boolean>(false)
    val user = MutableLiveData<Boolean>(false)
    fun categoryFilter() {
        var result = mutableListOf<MarkerModel>()

        if (filterCategory.value == null) {
            result = rawMarkerList.value!!
        } else {
            for (i in rawMarkerList.value!!.iterator()) {
                if (i.category == filterCategory.value) {
                    result.add(i)
                }
            }
        }

        displayMarkerList.postValue(result)
    }

    init {
        fetchData()
    }

    private fun fetchData() {
        CoroutineScope(Dispatchers.Main).launch {

        }
    }

    fun addMarkerToMap(markerModel: MarkerModel) {
      //  rawMarkerList.value!!.add(markerModel)
        categoryFilter()
        markerInsertDao(markerModel)
    }

    fun registerUser(email: String, password: String, username: String) {
        CoroutineScope(Dispatchers.IO).launch {
            ServiceLocator.realmManager.register(username, password)
            loggedIn.postValue(true)
        }
    }

    fun loginUser(email: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            ServiceLocator.realmManager.login(email, password)
            ServiceLocator.configureRealm()
            loggedIn.postValue(true)
        }
    }

    fun logout() {
        CoroutineScope(Dispatchers.IO).launch {
            ServiceLocator.realmManager.logout()
            loggedIn.postValue(false)
        }
    }

    fun markerInsertDao(marker: MarkerModel) {
        itemsDao.insertMarker(marker)
    }


}