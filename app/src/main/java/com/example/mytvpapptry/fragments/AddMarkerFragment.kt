package com.example.mytvpapptry.fragments

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mytvpapptry.Categories
import com.example.mytvpapptry.R
import com.example.mytvpapptry.databinding.FragmentAddMarkerBinding
import com.example.mytvpapptry.models.Position.MarkerModel
import com.example.mytvpapptry.viewmodel.ViewModel
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class AddMarkerFragment : Fragment() {
    lateinit var binding: FragmentAddMarkerBinding
    private val viewModel: ViewModel by activityViewModels()

    companion object {
        private const val TAG = "CameraXBasic"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val REQUEST_CODE_PERMISSIONS = 10
        private val REQUIRED_PERMISSIONS = arrayOf(android.Manifest.permission.CAMERA)
    }

    private var imageCapture: ImageCapture? = null
    private lateinit var outputDirectory: File
    private lateinit var cameraExecutor: ExecutorService

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddMarkerBinding.inflate(layoutInflater)

        this.binding.cameraView.implementationMode = PreviewView.ImplementationMode.COMPATIBLE

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            ActivityCompat.requestPermissions(
                requireActivity(),
                AddMarkerFragment.REQUIRED_PERMISSIONS,
                AddMarkerFragment.REQUEST_CODE_PERMISSIONS
            )
        }

        binding.cameraCaptureButton.setOnClickListener {
            takePhoto()
            saveMarker()
        }
        outputDirectory = getOutputDirectory()
        cameraExecutor = Executors.newSingleThreadExecutor()

        binding.buttonOpenGallery.setOnClickListener {
            openGalleryForImages()
        }

        return binding.root
    }

    private fun openGalleryForImages() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "image/*"
        resultLauncher.launch(intent)
    }

    var resultLauncher =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == Activity.RESULT_OK) {
                val data: Intent? = result.data
                val imageList = listOf<ImageView>(binding.imgView)

                if (data?.clipData != null) {
                    var count = data.clipData?.itemCount
                    for (i in 0 until count!!) {
                        var imageUri: Uri = data.clipData?.getItemAt(i)!!.uri
                        imageList[i].setImageURI(imageUri)
                    }
                } else if (data?.data != null) {
                    var imageUri: Uri = data.data!!

                }

            }
        }

    private fun saveMarker() {

    }

    private fun takePhoto() {
        val imageCapture = imageCapture ?: return
        val photoFile = File(
            outputDirectory,
            SimpleDateFormat(
                AddMarkerFragment.FILENAME_FORMAT, Locale.US
            ).format(System.currentTimeMillis()) + ".jpg"
        )
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()
        imageCapture.takePicture(outputOptions,
            ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(AddMarkerFragment.TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = Uri.fromFile(photoFile)

                    var category: Categories? = null
                    when (binding.categoriesSpinner.selectedItem.toString()) {
                        "Service Type" -> category = Categories.LifOff
                        "Private Car" -> category = Categories.PrimaryStage
                        "Taxi Car" -> category = Categories.SecondaryStage
                    }

                    val marker =
                        MarkerModel(
                            viewModel.selectedPosition.value!!,
                            binding.titleEditText.text.toString(),
                            binding.descriptionEditText.text.toString(),
                            Calendar.getInstance().time.toString(),
                            savedUri,
                            category!!
                        )

                    viewModel.addMarkerToMap(marker)
                    viewModel.markerInsertDao(marker)
                    println(savedUri)

                    findNavController().navigate(R.id.addmarker_to_map)
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }

    private fun getOutputDirectory(): File {
        val mediaDir = requireContext().externalMediaDirs.firstOrNull()?.let {
            File(it, resources.getString(R.string.app_name)).apply { mkdirs() }
        }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else requireContext().filesDir
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())
        cameraProviderFuture.addListener(
            Runnable {
                val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
                val preview =
                    Preview.Builder().build().also {
                        it.setSurfaceProvider(binding.cameraView.surfaceProvider)
                    }
                imageCapture = ImageCapture.Builder().build()
                val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(this, cameraSelector, preview, imageCapture)
            },
            ContextCompat.getMainExecutor(requireContext())
        )
    }

    private fun allPermissionsGranted() = AddMarkerFragment.REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }
}

