package com.example.mytvpapptry.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mytvpapptry.R
import com.example.mytvpapptry.ServiceLocator
import com.example.mytvpapptry.databinding.FragmentLoginBinding
import com.example.mytvpapptry.viewmodel.ViewModel

class LoginFragment : Fragment() {

    lateinit var binding: FragmentLoginBinding
    private val viewModel: ViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentLoginBinding.inflate(layoutInflater)

        binding.loginButton.setOnClickListener {
            findNavController().navigate(R.id.login_to_map)
        }

        binding.createAccountButton.setOnClickListener {
            findNavController().navigate(R.id.login_to_register)
        }

        binding.forgotPassword.setOnClickListener {
            findNavController().navigate(R.id.RegisterFragment)
        }

        return binding.root


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loggedIn.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(R.id.login_to_splash)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener {
            try {
                val email: String = binding.emailLogin.text.toString()
                val password: String = binding.passwordLogin.text.toString()
                viewModel.loginUser(email, password)
                ServiceLocator.configureRealm()
                val id = requireActivity().findViewById<TextView>(R.id.textView_email)
                val username = requireActivity().findViewById<TextView>(R.id.usernameEditText)
                id.text = email
                username.text = email
            } catch (ex: Exception) {
                Toast.makeText(activity, "login", Toast.LENGTH_SHORT).show()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
    }

}