package com.example.mytvpapptry.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.mytvpapptry.R
import com.example.mytvpapptry.viewmodel.SplashViewModel
import com.example.mytvpapptry.viewmodel.ViewModel

/**
 * Actividad Splash
 * @property TIME Long
 */
class SplashFragment : Fragment() {

    companion object {
        fun newInstance() = SplashFragment()
    }

    private val TIME = 100000L
    private lateinit var viewModel: SplashViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_splash, container, false)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this)[SplashViewModel::class.java]
//        viewModel.items.observe(viewLifecycleOwner){list ->
//            val listAsString = list.map { it.summary }.joinToString("\n")
//            requireView().findViewById<TextView>(R.id.list).text = listAsString
//        }

        viewModel.loggedIn.observe(viewLifecycleOwner) { loggedIn ->
            if (loggedIn) {
                findNavController().navigate(R.id.splash_to_map)
            } else {
                findNavController().navigate(R.id.splash_to_login)
            }


        }
        viewModel.start()
    }

}