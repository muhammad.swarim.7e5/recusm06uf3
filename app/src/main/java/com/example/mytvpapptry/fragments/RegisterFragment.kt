package com.example.mytvpapptry.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.mytvpapptry.R
import com.example.mytvpapptry.databinding.FragmentRegisterBinding
import com.example.mytvpapptry.viewmodel.ViewModel

class RegisterFragment : Fragment() {
    lateinit var binding: FragmentRegisterBinding
    private val viewModel: ViewModel by activityViewModels()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentRegisterBinding.inflate(layoutInflater)

        binding.createAccountButton.setOnClickListener {
            findNavController().navigate(R.id.LoginFragment)
        }
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.loggedIn.observe(viewLifecycleOwner){
            if(it){
                findNavController().navigate(R.id.login_to_splash)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.loginButton.setOnClickListener{
            val username : String = binding.emailUser.text.toString()
            val email : String = binding.emailRegister.text.toString()
            val password : String = binding.emailPassword.text.toString()
            viewModel.registerUser(email,password,username)
        }
    }
}