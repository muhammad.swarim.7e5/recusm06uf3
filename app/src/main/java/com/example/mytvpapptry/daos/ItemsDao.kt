package com.example.mytvpapptry.daos

import com.example.mytvpapptry.models.Position.MarkerModel
import com.example.mytvpapptry.realms.Items
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class ItemsDao(val realm: Realm, val userId: String) {


fun listFlow() : Flow<List<Items>> = realm.query<Items>().find().asFlow().map { it.list.toList() }

    fun insertMarker(m: MarkerModel) {
        realm.writeBlocking {
            val marker = Items(
                position = m.position,
                title = m.title,
                description = m.description,
                date = m.date,
                category = m.category.toString(),
                photoUri = m.photoUri.toString().toByteArray(),
                owner_id = userId
            )
            copyToRealm(marker)
        }
    }
}