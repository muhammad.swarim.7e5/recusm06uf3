package com.example.mytvpapptry.models.Position

data class Position(val latitude: Double, val longitude: Double) {

}