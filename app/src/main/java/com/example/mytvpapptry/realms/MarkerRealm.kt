package com.example.mytvpapptry.realms

import com.example.mytvpapptry.models.Position.Position
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey


open class Items(
    @PrimaryKey var _id: ObjectId = ObjectId.create(),
    var position: Position = Position(-0.1, -0.1),
    var title: String = "",
    var description: String = "",
    var date: String = "",
    var photoUri: ByteArray? = null,
    var category: String = "",
    var owner_id: String = ""
) : RealmObject {
    constructor() : this(owner_id = "") {
    }
}


