package com.example.mytvpapptry

import com.example.mytvpapptry.daos.ItemsDao
import com.example.mytvpapptry.daos.RealmManager
import com.example.mytvpapptry.models.Position.MarkerModel
import kotlinx.coroutines.flow.first

object ServiceLocator {
    val realmManager = RealmManager()
    lateinit var itemsDao: ItemsDao


    fun configureRealm() {
        requireNotNull(realmManager.realm)
        val realm = realmManager.realm!!
        itemsDao = ItemsDao(realm, realmManager.realmApp.currentUser!!.id)
    }

//    // Configuración de Realm y ItemsDao
//    suspend fun setupRealmAndItemsDao() {
//        ServiceLocator.configureRealm() // Configurar la instancia de Realm
//        val realm = ServiceLocator.realmManager.realm!!
//        val userId = ItemsDao(realm, realmManager.realmApp.currentUser!!.id).userId
//        val itemsDao = ItemsDao(realm, userId) // Configurar la instancia de ItemsDao
//        // Usar itemsDao para interactuar con los datos de Realm
//        itemsDao.insertMarker(MarkerModel())
//    }

}