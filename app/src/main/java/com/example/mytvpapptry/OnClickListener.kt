package com.example.mytvpapptry

import com.example.mytvpapptry.models.Position.MarkerModel


interface OnClickListener {
    fun onClick(marker: MarkerModel)
}

